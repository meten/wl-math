import time
import operator
ops = { "+": operator.add,
        "-": operator.sub,
        "*": operator.mul,
        "/": operator.truediv,
        "%": operator.mod }

def eval_binary_expr(op1, oper, op2, get_operator_fn=ops.get):
    op1, op2 = int(op1), int(op2)
    return get_operator_fn(oper)(op1, op2)

def watch(fn, words):
    fp = open(fn, 'r', encoding="utf8")
    while True:
        new = fp.readline()
        # Once all lines are read this just returns ''
        # until the file changes and a new line appears

        if new:
            for word in words:
                if word in new:
                    yield (word, new)
        else:
            time.sleep(0.5)

fn = '../console.log'
words = ['\x07ffffff[\x07FF69B4Math\x07ffffff]\x07FFFFFF']
for hit_word, hit_sentence in watch(fn, words):
    print("Found %r in line: %r" % (hit_word, hit_sentence))
    i = 35
    eq = ""
    while i < len(hit_sentence):
        print (hit_sentence[i])
        if hit_sentence[i] == '=':
            break
        eq += hit_sentence[i]
        i+=1
    res = (eval_binary_expr(*(eq.split())))
    print(res)
    f = open("wlmath.cfg", "w")
    f.write("say {}".format(res))
    f.close()
    time.sleep(1)
    #f = open("wlmath.cfg", "w")
    #f.truncate(0)

